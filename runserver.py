from flask import Flask, request, jsonify
from pymongo import MongoClient
from bson import ObjectId, Timestamp

app = Flask(__name__)

# Will connect to mongodb running on localhost

mdb = MongoClient('localhost', 27017)
cookie_db = mdb.cookie_db
cookies = cookie_db.cookies

test_cookie = {'user_id', 'flavor', 'temp', 'radius', 'deliciousness',
               'toppings', 'date_baked', 'fresh'}
avail_params = {'limit', 'order', 'order_by'}
avail_order_by = {'flavor', 'temp', 'radius', 'deliciousness', 'date_baked'}

# app routes with accompanying methods go here

@app.route('/')
def index():
    return 'Welcome to Cookies API!'


# error check cookie values
def type_check(cookie):
    e = ''

    # for posting cookie: all keys should be present
    # for updating cookie: only certain keys present

    if 'user_id' in cookie:
        try:
            int(cookie['user_id'])
        except ValueError:
            pass
        else:
            e += 'user_id must be of type str, '

    if 'flavor' in cookie:
        try:
            int(cookie['flavor'])
        except ValueError:
            pass
        else:
            e += 'flavor must be of type str, '

    if 'temp' in cookie:
        try:
            int(cookie['temp'])
        except ValueError:
            e += 'temp must be of type int, '
        else:
            cookie['temp'] = int(cookie['temp'])

    if 'radius' in cookie:
        # tricky here because int can pass as float
        try:
            float(cookie['radius'])
        except ValueError:
            e += 'radius must be of type float, '
        else:
            cookie['radius'] = float(cookie['radius'])

    if 'deliciousness' in cookie:
        try:
            int(cookie['deliciousness'])
        except ValueError:
            e += 'deliciousness must be of type int and a value from 1 - 10, '
        else:
            if int(cookie['deliciousness']) < 1 or \
                int(cookie['deliciousness']) > 10:
                e += 'deliciousness must a value from 1 - 10, '
            else:
                cookie['deliciousness'] = int(cookie['deliciousness'])

    if 'toppings' in cookie:
        if cookie['toppings'] != 'True' and cookie['toppings'] != 'False':
            e += 'toppings must be of type bool, '

    if 'date_baked' in cookie:
        try:
            int(cookie['date_baked'])
        except ValueError:
            e += 'date_baked must be of type int, '
        else:
            cookie['date_baked'] = int(cookie['date_baked'])

    if 'fresh' in cookie:
        if cookie['fresh'] != 'True' and cookie['fresh'] != 'False':
            e += 'fresh must be of type bool, '

    return e


# validate cookie
def validate_new_cookie(cookie):

    # check cookie has correct keys
    if set(cookie) != set(test_cookie):
        incorrect = set(cookie) - set(test_cookie)
        missing = set(test_cookie) - set(cookie)
        if incorrect and missing:
            return jsonify(error=400, message='Missing key(s): ' +
             str(list(missing)) + ', Incorrect key(s): ' +
              str(list(incorrect))), 400
        elif missing:
            return jsonify(error=400, message='Missing key(s): ' +
             str(list(missing))), 400
        elif incorrect:
            return jsonify(error=400, message='Incorrect key(s): ' +
             str(list(incorrect))), 400
        else:
            return 'ERROR', 400
    else:
        # value type checking
        e = type_check(cookie)

        if e:
            return jsonify(error=400, message=e[0:-2]), 400
        else:
            inserted_id = cookies.insert_one(cookie).inserted_id
            c = cookies.find_one({'_id':inserted_id})
            c['_id'] = str(c['_id'])
            return jsonify(message='Successfully posted cookie', cookie=c), 200


# validate cookie for update
def update_cookie(cookie, cookie_id):

    # make sure no extra keys, or update id, or empty body
    if '_id' in cookie:
        return jsonify(error=401, message='Cannot change cookie id'), 401

    elif set(cookie) - set(test_cookie):
        return jsonify(error=400, message='Incorrect key(s): ' +
         str(list(set(cookie) - set(test_cookie)))), 400

    elif set(test_cookie) - set(cookie) == set(test_cookie):
        return jsonify(error=400, message='Must provide values to update'), 400

    else:
        # type check update values
        e = type_check(cookie)
        if e:
            return jsonify(error=400, message=e[0:-2]), 400
        else:
            c = cookies.find_one({'_id':ObjectId(cookie_id)})
            for key, value in cookie.items():
                c[key] = cookie[key]
            cookies.update({'_id':ObjectId(cookie_id)}, c)

            c['_id'] = str(c['_id'])
            return jsonify(message='Successfully updated cookie', cookie=c), 200


# validate limit param
def check_limit(limit):
    try:
        int(limit)
    except ValueError:
        return jsonify(error=400, message='limit must a non-zero,'
         + ' positive int'), 400
    else:
        if int(limit) < 1:
            return jsonify(error=400, message='limit must a non-zero,'
             + ' positive int'), 400


# if a get cookies request has parameters
def special_params(args, user_id):

    # wrong params
    if set(args) - set(avail_params):
        return jsonify(error=400, message='Unsupported param(s): ' +
          str(list(set(args) - set(avail_params)))), 400

    # incomplete params
    elif 'order' in args and not 'order_by' in args:
        return jsonify(error=400, message='Must specify which parameter'
         + ' to order with order_by=<key>'), 400

    # incomplete params
    elif not 'order' in args and 'order_by' in args \
        or ('order' in args and 'order_by' in args \
        and args['order'] != 'asc' and args['order'] != 'desc'):
        return jsonify(error=400, message='Must specify order parameter:'
         + ' order=asc or order=desc'), 400

    # order and order_by
    elif 'order' in args and 'order_by' in args:

        if args['order'] == 'asc':
            order = 1
        else:
            order = -1

        # wrong param to order by
        if not set({args['order_by']}) & set(avail_order_by):
            return jsonify(error=400, message='order_by must be one of the'
             + ' following: ' + str(list(avail_order_by))), 400

        # also limit
        elif 'limit' in args:
            check_limit(args['limit'])
            # limit ok
            cookie_list = list(cookies.find({'user_id': \
             request.headers['user_id']}).sort(args['order_by'], order) \
             .limit(int(args['limit'])))

        # no limit
        else:
            cookie_list = list(cookies.find({'user_id': \
             request.headers['user_id']}).sort(args['order_by'], order))

    # just limit
    else:
        check_limit(args['limit'])
        cookie_list = list(cookies.find({'user_id': \
         request.headers['user_id']}).limit(int(args['limit'])))

    for c in cookie_list:
        c['_id'] = str(c['_id'])

    return jsonify(cookie_list), 200


# main route for creating new cookies or querying all or specific ones
@app.route('/cookies/', methods=['GET', 'POST'])
def cookie_time():

    if request.method == 'POST':

        if 'user_id' not in request.headers:
            return jsonify(error=400, text='user_id required in request' +
             ' header'), 400

        cookie = request.form.to_dict()

        if 'user_id' in cookie and request.headers['user_id'] \
            != cookie['user_id']:
            return jsonify(error=403, text='user_id for cookie must match'
             + ' user_id in header'), 403

        return validate_new_cookie(cookie)

    elif request.method == 'GET':

        # user_id is in header and user_id exists in db
        if 'user_id' not in request.headers:
            return jsonify(error=400, text='user_id required in request' +
             ' header'), 400
        elif cookies.find({'user_id':request.headers['user_id']}).count() == 0:
            return jsonify(error=403, text='user_id must exist in the' +
             ' database'), 403
        else:

            # if parameters are given
            args = request.args.to_dict()
            if args:
                return special_params(args, request.headers['user_id'])

            # no parameters, get all cookies
            else:
                all_cookies = list(cookies.find({'user_id': \
                 request.headers['user_id']}))
                for c in all_cookies:
                    c['_id'] = str(c['_id'])
                return jsonify(all_cookies), 200

    else:
        return 'ERROR', 400


# route for getting, updating, and deleting single cookies
@app.route('/cookies/<string:cookie_id>', methods=['GET', 'PUT', 'DELETE'])
def cookie_exists(cookie_id):

    # user_id is in header and user_id exists in db
    if 'user_id' not in request.headers:
        return jsonify(error=400, text='user_id required in request' +
         ' header'), 400
    elif cookies.find({'user_id':request.headers['user_id']}).count() == 0:
        return jsonify(error=403, text='user_id must exist in the' +
         ' database'), 403
    else:
        if ObjectId.is_valid(cookie_id):
            c = cookies.find_one({'_id':ObjectId(cookie_id)})
        else:
            return jsonify(error=404, message=cookie_id
             + ' is not a valid cookie id. Must be 12-byte hex value'), 404
        if c:
            if request.headers['user_id'] != c['user_id']:
                return jsonify(error=403, text='user_id for cookie must match'
                 + ' user_id in header'), 403

            if request.method == 'GET':
                c['_id'] = str(c['_id'])
                return jsonify(cookie=c), 200
            elif request.method == 'PUT':
                new_cookie = request.form.to_dict()
                return update_cookie(new_cookie, cookie_id)
            elif request.method == 'DELETE':
                cookies.delete_one({'_id':ObjectId(cookie_id)})
                return jsonify(message='Successfully deleted cookie with id: '
                 + cookie_id), 200
            else:
                return 'ERROR', 400
        else:
            return jsonify(error=404, \
             message='Cookie with id %s not found' % cookie_id), 404



if __name__ == '__main__':
    HOST = '0.0.0.0'
    PORT = 5555

    app.run(HOST, PORT, debug = True)

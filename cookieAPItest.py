import unittest
import json
import requests


url = 'http://localhost:5555/cookies/'


class TestGetAllCookies(unittest.TestCase):

    def test_all_cookies_no_user_id(self):

        response = requests.request("GET", url)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = json.dumps({"error": 400, "text": "user_id required in" +
         " request header"})
        self.assertEqual(response, expected)

    def test_all_cookies_wrong_user_id(self):

        headers = {'user_id': 'nope'}

        response = requests.request("GET", url, headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = json.dumps({"error": 403, "text": "user_id must exist in" +
         " the database"})
        self.assertEqual(response, expected)

    def test_all_cookies_user_id(self):

        headers = {'user_id': 'dood'}

        response = requests.request("GET", url, headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = '"_id": "5b18968488cf2c813f6b0859"'
        self.assertIn(expected, response)

    def test_all_cookies_params(self):

        querystring = {"order_by":"temp","limit":"10","order":"asc"}
        headers = {'user_id': 'dood'}

        response = requests.request("GET", url, headers=headers,
         params=querystring)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = '"_id": "5b1896d488cf2c813f6b085c"'
        self.assertIn(expected, response)

        # isn't an accurate test because I need to assert that certain cookies
        # are listed before other ones, making sure the ordering works

class TestPostNewCookies(unittest.TestCase):

    def test_new_cookie_no_user_id(self):

        body = {'flavor': 'snicker doodle', 'temp': 113, 'radius': 36.0,
                'deliciousness': 10, 'toppings': False,
                'date_baked': 10790343458, 'fresh': True}

        response = requests.request("POST", url, json=body)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = json.dumps({"error": 400, "text": "user_id required in" +
         " request header"})
        self.assertEqual(expected, response)


    def test_new_cookie_no_user_id_header(self):

        headers = {'user_id': 'dood'}
        body = {'flavor': 'peanut butter', 'temp': 150, 'radius': 2,
                'deliciousness': 9, 'toppings': False,
                'date_baked': 10790343129, 'fresh': True}

        response = requests.request("POST", url, data=body, headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = json.dumps({"error": 400, "message": "Missing key(s):" +
         " ['user_id']"})
        self.assertEqual(expected, response)


    def test_new_cookie_user_id_no_match(self):

        body = {'flavor': 'snicker doodle', 'temp': 113, 'radius': 36.0,
                'deliciousness': 10, 'toppings': False,
                'date_baked': 10790343458, 'fresh': True, 'user_id': 'dood3'}
        headers = {'user_id': 'dood'}

        response = requests.request("POST", url, data=body, headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = json.dumps({"error": 403, "text": "user_id for cookie" +
         " must match user_id in header"})
        self.assertEqual(expected, response)

    def test_new_cookie_ok(self):

        body = {'flavor': 'snicker doodle', 'temp': 102, 'radius': 5.3,
                'deliciousness': 9, 'toppings': False,
                'date_baked': 10790341111, 'fresh': True, 'user_id': 'dood1'}
        headers = {'user_id': 'dood1'}

        response = requests.request("POST", url, data=body, headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = '"message": "Successfully posted cookie"'
        self.assertIn(expected, response)


class TestGetPutDeleteCookies(unittest.TestCase):

    def test_get_one_cookie_wrong_user_id(self):

        headers = {'user_id': 'dood1'}

        response = requests.request("GET", url + "5b18968488cf2c813f6b0859",
         headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = json.dumps({"error": 403, "text": "user_id for cookie" +
         " must match user_id in header"})
        self.assertEqual(expected, response)


    def test_get_one_cookie(self):

        headers = {'user_id': 'dood'}

        response = requests.request("GET", url + "5b18968488cf2c813f6b0859",
         headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = '"_id": "5b18968488cf2c813f6b0859"'
        self.assertIn(expected, response)


    def test_update_cookie_wrong_user_id(self):

        headers = {'user_id': 'dood1'}
        body = {'deliciousness': 10}

        response = requests.request("PUT", url + "5b18968488cf2c813f6b0859",
         data=body, headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = json.dumps({"error": 403, "text": "user_id for cookie" +
         " must match user_id in header"})
        self.assertEqual(expected, response)


    def test_update_cookie(self):

        headers = {'user_id': 'dood'}
        body = {'deliciousness': 9}

        response = requests.request("PUT", url + "5b18968488cf2c813f6b0859",
         data=body, headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = '"message": "Successfully updated cookie"'
        self.assertIn(expected, response)


    def test_delete_cookie_doesnt_exist(self):

        headers = {'user_id': 'dood'}

        response = requests.request("DELETE", url + "5b15f71188cf2ccdbe82d03b",
         headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = json.dumps({"error": 404, "message": "Cookie with id" +
         " 5b15f71188cf2ccdbe82d03b not found"})
        self.assertEqual(expected, response)


    def test_delete_cookie_wrong_user_id(self):

        headers = {'user_id': 'dood1'}

        response = requests.request("DELETE", url + "5b1896d488cf2c813f6b085c",
         headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = json.dumps({"error": 403, "text": "user_id for cookie" +
         " must match user_id in header"})
        self.assertEqual(expected, response)


    def test_create_and_delete_cookie(self):

        body = {'flavor': 'bad', 'temp': 12, 'radius': 1.5,
                'deliciousness': 1, 'toppings': False,
                'date_baked': 10790341123, 'fresh': False, 'user_id': 'dood1'}
        headers = {'user_id': 'dood1'}

        response = requests.request("POST", url, data=body, headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        s = '"_id": '
        cookie_id = response[response.find(s) + len(s) + 1 : response.find(s) \
         + len(s) + 25]

        response = requests.request("DELETE", url + cookie_id, headers=headers)
        response = response.text.replace('\n', '').replace('  "', '"')
        expected = '"message": "Successfully deleted cookie with id: ' \
         + cookie_id + '"'
        self.assertIn(expected, response)



if __name__ == "__main__":
    unittest.main()

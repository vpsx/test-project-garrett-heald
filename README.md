# Cookies API

You are tasked with creating a minimal REST API which provides endpoints to enable CRUD operations on cookies . . . no, not browser cookies, actual cookies (chocolate chip, peanut butter, sugar, etc).

You will develop the endpoints and methods for the API so a user can manage their cookie (object) stash via HTTP calls. You will also provide documentation in this README about how to formulate those calls to the endpoints
so the user can clearly understand how to manage their cookies. Data about the cookie objects will persist in an instance of MongoDB under a database called, interestingly enough, `cookies`.

The user should be able to use the API to create new cookies, edit their properties (you get to decide which properties they have), delete (eat) cookies, and get information about all their cookies. For example, how many chocolate chip cookies do I have right now?

As an example, a `cookie` object could look like this in the db:

```
{
    'user_id': 'dood',
	'flavor': 'snicker doodle',
	'temp': 113,
	'radius': 3.4,
	'deliciousness': 10,
	'toppings': false,
	'date_baked': 10790343458,
	'fresh': true
}
```

An interesting endpoint might be something like `http://cookieapi.com/topDelicious/10`, which returns the top 10 most delicious cookies belonging to a user. Your creativity and sky is the limit, but don't spend too much time on it :)

You will be using Python 3.x with Flask and pymongo. Set up a development environment on your machine, and when you submit the project you can provide us with a dump of the data in your `cookies` db.

We'll be looking at the following criteria:

- How intuitive it is to use
- Object-oriented approach
- Logical consistency of HTTP methods and their resulting actions and responses
- Conciseness and clarity
- Error handling
- Use of git to manage development flow
- Extra points for creativity and interesting features
- Extra points for test cases

Don't worry about security--authentication, authorization, etc, but a user should have to provide their `user_id` or similar identifier with every call to the API.

Contact ben.coleman@savantx.com with any questions. Good luck!




# Cookies API Documentation

## How to run Cookies API
Run the server: `python3 runserver.py`.
The web app will then run on url=<http://localhost:5555/>.

Run the test suite: `python3 -m unittest cookieAPItest.py`

For all calls made to the API, 'user\_id' must be present in the header, and correspond to a real user\_id from cookies in the database. For example, for a new user to post a new cookie, the new user\_id in the request body must match the new user\_id in the header.  

For all GET, PUT, and DELETE requests, the user must have access to the cookie: the cookie wanted has the same user\_id as the one placed in the header.  

All Response bodies are formed in JSON notation, including error or success codes.

## Available Routes

### /cookies/
####GET
>#####Without Parameters  
>>__GET__ url/cookies/  
>>_Get all cookies belonging to user_
>>>header = `{user_id: <user_id>}`  

>>>__Returns__: list of all cookie objects that belong to \<user\_id\>   

>>>__Possible Errors__:  
>>>>400 - user\_id not in request header  
>>>>403 - user\_id doesn't exist in the database
 
>#####With Parameters
>>__GET__ url/cookies/?order_by=deliciousness&order=desc&limit=10  
>>_Get list of cookies belonging to user with special parameters_  
>>>header = `{user_id: <user_id>}`  
>>>
>>>__Returns__:   list of 10 cookie objects that belong to \<user\_id\> sorted by descending deliciousness  

>>>__Possible Errors__:  
>>>>400 - user\_id not in request header  
>>>>400 - Parameters are incorrect  
>>>>403 - user\_id doesn't exist in the database

>>>__Available Parameters:__  
>>>>__limit__ = [int > 0]  
>>>>__order\_by__ = [flavor, temp, radius, deliciousness, date_baked]  
>>>>__order__ = [asc, desc]  

>>>_NOTE: order and order\_by must both be present if used_

####POST
>__POST__ url/cookies/  
>_Add a new cookie to the database_
>
>>header = `{user_id: <user_id>}`  
>>body = 

>>
```
{
    'user_id': 'dood',
	'flavor': 'snicker doodle',
	'temp': 113,
	'radius': 3.4,
	'deliciousness': 10,
	'toppings': False,
	'date_baked': 10790343458,
	'fresh': True
}
```  

>>__Returns__: Cookie object that has just been posted with success message or error  

>>__Possible Errors__:  
>>>400 - user\_id not in request header  
>>>400 - Cookie keys or value types are incorrect  
>>>403 - Cookie user\_id does not match header user\_id  

### /cookies/{\_id}
####GET  

>__GET__ url/cookies/{\_id}  
>_Get a specific cookie with {\_id}_
>>header = `{user_id: <user_id>}`  

>>__Returns__: Cookie object that belongs to \<user\_id\> with cookie id {\_id} or error  

>>__Possible Errors__:  
>>>400 - user\_id not in request header  
>>>403 - user\_id doesn't exist in the database  
>>>403 - user\_id in header doesn't match user\_id of requested cookie  
>>>404 - cookie {\_id} isn't a 12-byte hex string  
>>>404 - no cookie exists in database with cookie {\_id}

####PUT

>__PUT__ url/cookies/{\_id}  
>_Update any field for cookie with {\_id} if it belongs to \<user\_id\>_  
>_NOTE: Cannot update '\_id' field of cookie_
>>header = `{user_id: <user_id>}`  
>>body = 

>>
```
{
	'flavor': 'chocolate chip',
	'deliciousness': 9,
	'toppings': True
}
```  

>>__Returns__: Updated cookie object with success message or error  

>>__Possible Errors__:
>>>400 - user\_id not in request header  
>>>400 - Cookie keys or value types are incorrect  
>>>403 - user\_id doesn't exist in the database  
>>>403 - user\_id in header doesn't match user\_id of requested cookie to update  
>>>404 - cookie {\_id} isn't a 12-byte hex string  
>>>404 - no cookie exists in database with cookie {\_id}

####DELETE
>__DELETE__ url/cookies/{\_id}  
_Delete cookie with {\_id} if it belongs to \<user\_id\>_
>>header = `{user_id: <user_id>}` 

>>__Returns__: Success message of cookie id {\_id} deletion or error  

>>__Possible Errors__:  
>>>400 - user\_id not in request header  
>>>403 - user\_id doesn't exist in the database  
>>>403 - user\_id in header doesn't match user\_id of requested cookie to delete  
>>>404 - cookie {\_id} isn't a 12-byte hex string  
>>>404 - no cookie exists in database with cookie {\_id}  

# Created by Garrett Heald